-- |

module Utility.Tweaks where

-- | Converts an 'Int' to 'Double'.
integerToDouble :: Int -> Double
integerToDouble = fromIntegral

-- | Rounds a number to two decimal places.
round2 :: Double -> Double
round2 num = sig + nonSig2 / 100
 where
  sig     = integerToDouble $ round num
  nonSig  = num - sig
  nonSig2 = integerToDouble $ round $ nonSig * 100

-- | Converts 'Just 0' to 'Nothing'.
fixMaybe :: Maybe Double -> Maybe Double
fixMaybe a | a == Just 0.0 = Nothing
           | otherwise     = a

-- | Tweaks 'Confidence' value for 'SQL'.
tweak :: Maybe Double -> Maybe Double
tweak = fixMaybe . fmap round2
