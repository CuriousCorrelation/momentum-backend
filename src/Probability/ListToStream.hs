listToStream :: [a] -> Stream a
listToStream = Stream next
    where
        next (x : xs) = Yield x xs
        next [] 	  = Done