module KalmanFilter 
    ( SystemModel(..)
    , System (..)
    , ObservationModel (..)
    , SystemState
    , SystemObservation
    , Kalman (..)
    , predict
    , update
    ) where


import Data.Packed
import Numeric.LinearAlgebra
import Numeric.Container ()

-- System model
data SystemModel = SystemModel {
        smTransitionModel :: Matrix Double,
        smNoiseCovariance :: Matrix Double
    } deriving (Show)

-- Observation model
data ObservationModel = ObservationModel {
        omModel           :: Matrix Double,
        omNoiseCovariance :: Matrix Double
    } deriving (Show)

-- Whole system
data System = System {
        sModel       :: SystemModel,
        sObservation :: ObservationModel
    } deriving (Show)

-- System current state
newtype SystemState = 
    SystemState (Vector Double) deriving (Eq, Show)

-- System observation
newtype SystemObservation = 
    SystemObservation (Vector Double) deriving (Eq, Show)

-- Kalman estimates
data Kalman = Kalman {
        kState      :: SystemState,
        kCovariance :: Matrix Double
    } deriving (Show)

-- Predict phase
predict :: System -> Kalman -> Kalman
predict 
    (System (SystemModel model noise) _) 
    (Kalman (SystemState state) covariance) = 
        Kalman (SystemState newState) newCovariance
        where
            newState      = model <> state
            newCovariance = model <> covariance <> trans model 
                + noise

-- Update phase
update :: System -> Kalman -> SystemObservation -> Kalman
update 
    (System _ (ObservationModel model noise)) 
    (Kalman (SystemState state) covariance) 
    (SystemObservation observation) =
        Kalman (SystemState newState) newCovariance
        where
            innovation           = observation - model <> state
            innovationCovariance = model <> covariance 
                <> trans model + noise
            gain                 = covariance <> trans model 
                <> inv innovationCovariance
            identity             = ident (dim state) :: Matrix Double
            newState             = state + gain <> innovation
            newCovariance        = (identity - gain <> model) 
                <> covariance
