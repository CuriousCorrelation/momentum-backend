-- |

module Postgres.Migration where

import           Database.Persist.Postgresql    ( ConnectionString
                                                , runMigration
                                                )
import           Database.Persist.Sql           ( Migration )
import           Utility.PersistentActions      ( runActionT )
import qualified Types.Confidence              as EConfidence
import qualified Types.Equity                  as EEquity


-- | Migrates database.
migrateDB :: ConnectionString -> Migration -> IO ()
migrateDB connectionString migrationInfo =
  runActionT connectionString (runMigration migrationInfo)

-- | Migrates 'Equity' database using 'defConnectionString'.
migrateDBEquity :: ConnectionString -> IO ()
migrateDBEquity connStr = runActionT connStr (runMigration EEquity.migrateAll)

-- | Migrates 'Confidence' database using 'defConnectionString'.
migrateDBConfidence :: ConnectionString -> IO ()
migrateDBConfidence connStr =
  runActionT connStr (runMigration EConfidence.migrateAll)
