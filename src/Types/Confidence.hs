{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE UndecidableInstances       #-}

-- |

module Types.Confidence where

import           Database.Persist.TH            ( mkMigrate
                                                , mkPersist
                                                , persistLowerCase
                                                , share
                                                , sqlSettings
                                                )
import           Data.Time                      ( Day )
import           Data.Aeson                     ( FromJSON
                                                , ToJSON
                                                )
import           GHC.Generics                   ( Generic )

-- | Confidence factor. Prediction - Actual (Linear Regression).
type ConfidenceRoR = Double

-- | Direction of the movement. (Trend. Upwards or Downwards)
type ConfidenceDirection = Double

-- | Closing price of 'Equity' for the most recent trading day.
type LatestClose = Double

-- | Average price of the 'Equity'.
type AverageClose = Double

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|
Confidence

    latestClose            LatestClose      Maybe

    historicAverageClose   AverageClose     Maybe
    yearAverageClose       AverageClose     Maybe
    monthAverageClose      AverageClose     Maybe

    historicRor            ConfidenceRoR    Maybe
    yearRor                ConfidenceRoR    Maybe
    monthRor               ConfidenceRoR    Maybe

    weightedRor            ConfidenceRoR    Maybe

    availableFrom          Day              Maybe
    availableTo            Day              Maybe
    equityIdentifier       String           Maybe
    equityTitle            String           Maybe
    equityTrades           Int              Maybe
    equityShares           Int              Maybe

    deriving Show Generic
|]

instance FromJSON Confidence

instance ToJSON Confidence

-- | MigrateAll

-- [bindTo] migrateAll
-- | Migration function for 'Confidence', 'ConfidenceYear' and 'ConfidenceMonth'.

-- | Confidence

-- [bindTo] Confidence
-- | Record that stores Confidence information of a list of Equity.

-- [bindTo] confidenceLatestClose
-- | Closing price of the most recent trading day.

-- [bindTo] confidenceAverageClose
-- | Average price of the 'Equity'.

-- [bindTo] confidenceRoR
-- | Confidence factor. Prediction - Actual (Linear Regression).

-- [bindTo] confidenceDirection
-- | Direction of the movement. (Trend. Upwards or Downwards)

-- [bindTo] confidenceBeta
-- | Beta of the movement. Higher the number, greater the trend.

-- [bindTo] confidenceAvailableFrom
-- | Day from which Confidence is built.

-- [bindTo] confidenceAvailableTo
-- | Day to which Confidence is built.

-- [bindTo] confidenceEquityIdentifier
-- | Identifier of the 'Equity' used to build this 'Confidence'.

-- [bindTo] confidenceEquityTitle
-- | Title of the 'Equity' used to build this 'Confidence'.

-- [bindTo] confidenceEquityTrades
-- | Number of trades done in the most recent trading day.

-- [bindTo] confidenceEquityShares
-- | Number of currently available shares.

-- | ConfidenceYear

-- [bindTo] ConfidenceYear
-- | Record that stores ConfidenceYear information of a list of Equity.

-- [bindTo] confidenceLatestClose
-- | Closing price of the most recent trading day.

-- [bindTo] confidenceAverageClose
-- | Average price of the 'Equity'.

-- [bindTo] confidenceRoR
-- | ConfidenceYear factor. Prediction - Actual (Linear Regression).

-- [bindTo] confidenceDirection
-- | Direction of the movement. (Trend. Upwards or Downwards)

-- [bindTo] confidenceBeta
-- | Beta of the movement. Higher the number, greater the trend.

-- [bindTo] confidenceAvailableFrom
-- | Day from which ConfidenceYear is built.

-- [bindTo] confidenceAvailableTo
-- | Day to which ConfidenceYear is built.

-- [bindTo] confidenceEquityIdentifier
-- | Identifier of the 'Equity' used to build this 'ConfidenceYear'.

-- [bindTo] confidenceEquityTitle
-- | Title of the 'Equity' used to build this 'ConfidenceYear'.

-- [bindTo] confidenceEquityTrades
-- | Number of trades done in the most recent trading day.

-- [bindTo] confidenceEquityShares
-- | Number of currently available shares.

-- | ConfidenceMonth

-- [bindTo] ConfidenceMonth
-- | Record that stores ConfidenceMonth information of a list of Equity.

-- [bindTo] confidenceLatestClose
-- | Closing price of the most recent trading day.

-- [bindTo] confidenceAverageClose
-- | Average price of the 'Equity'.

-- [bindTo] confidenceRoR
-- | ConfidenceMonth factor. Prediction - Actual (Linear Regression).

-- [bindTo] confidenceDirection
-- | Direction of the movement. (Trend. Upwards or Downwards)

-- [bindTo] confidenceBeta
-- | Beta of the movement. Higher the number, greater the trend.

-- [bindTo] confidenceAvailableFrom
-- | Day from which ConfidenceMonth is built.

-- [bindTo] confidenceAvailableTo
-- | Day to which ConfidenceMonth is built.

-- [bindTo] confidenceEquityIdentifier
-- | Identifier of the 'Equity' used to build this 'ConfidenceMonth'.

-- [bindTo] confidenceEquityTitle
-- | Title of the 'Equity' used to build this 'ConfidenceMonth'.

-- [bindTo] confidenceEquityTrades
-- | Number of trades done in the most recent trading day.

-- [bindTo] confidenceEquityShares
-- | Number of currently available shares.
