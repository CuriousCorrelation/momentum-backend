-- |

module Charting.Easier where

import           Data.Colour.SRGB               ( sRGB )
import           Graphics.Rendering.Chart.Axis.Types
                                                ( PlotValue )
import           Graphics.Rendering.Chart.Backend.Diagrams
                                                ( toFile )
import           Graphics.Rendering.Chart.Easy  ( AlphaColour
                                                , EC
                                                , plot
                                                , opaque
                                                , (.=)
                                                , line
                                                , def
                                                , layout_title
                                                , setColors
                                                )
import           System.Directory               ( createDirectoryIfMissing )

lineColor1 :: AlphaColour Double
lineColor1 = opaque $ sRGB 0.25 1 0.25

lineColor2 :: AlphaColour Double
lineColor2 = opaque $ sRGB 0.25 0.25 1

lineColor3 :: AlphaColour Double
lineColor3 = opaque $ sRGB 1 0.25 0.25

zipBaseLine :: [a] -> [(Double, a)]
zipBaseLine = zip [0, 1 ..]

chartAutoCorrelation :: PlotValue y => ([y], [y], [y]) -> IO ()
chartAutoCorrelation (autoCorrL, autoCorrM, autoCorrU) = do
  createDirectoryIfMissing True "charts"
  toFile def "charts/autocorr.svg" $ do
    layout_title .= "AutoCorrelation"
    setColors [lineColor1, lineColor2, lineColor3]
    plot (line "autoCorr" [zipBaseLine autoCorrL])
    plot (line "autoCorr lower bound" [zipBaseLine autoCorrM])
    plot (line "autoCorr upper bound" [zipBaseLine autoCorrU])
