-- |
module Types.CLIOptions where

data MomentumCLI =
  MomentumCLI
    { runPopulation :: Bool
    , runLRBenchmark :: Bool
    , runWBenchmark :: Bool
    , runTest :: Bool
    }
