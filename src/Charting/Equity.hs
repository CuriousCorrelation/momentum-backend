{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}

-- |
module Charting.Equity where

import           Types.Momentum                 ( toParallelProcess
                                                , MomentumConfig
                                                )
import           Graphics.Rendering.Chart.Plot.Lines
                                                ( PlotLines )
import           Control.Monad                  ( forM_ )
import           Control.Monad.Reader           ( asks
                                                , lift
                                                , MonadReader
                                                )
import           Data.List.Split                ( chunksOf )
import           Control.Monad.IO.Class         ( MonadIO
                                                , liftIO
                                                )
import           Control.Monad.Reader           ( ReaderT )
import           Data.Colour.SRGB               ( sRGB )
import           Data.Maybe                     ( fromJust
                                                , fromMaybe
                                                , isJust
                                                )
import           Data.Time                      ( Day )
import           Database.Persist.Sql           ( BackendCompatible
                                                , BaseBackend
                                                , PersistEntity
                                                , PersistEntityBackend
                                                , PersistQueryRead
                                                , SqlBackend
                                                )
import           Graphics.Rendering.Chart.Backend.Diagrams
                                                ( toFile )
import           Graphics.Rendering.Chart.Easy  ( AlphaColour
                                                , EC
                                                , PlotFillBetween
                                                , (.=)
                                                , def
                                                , layoutlr_title
                                                , liftEC
                                                , line
                                                , opaque
                                                , plotLeft
                                                , plotRight
                                                , plot_fillbetween_style
                                                , plot_fillbetween_title
                                                , plot_fillbetween_values
                                                , setColors
                                                , solidFillStyle
                                                , takeColor
                                                )
import           Retrive.Equity.Database        ( getListOfActiveEquityIdentifiers
                                                , getListOfEquityFromEquityIdentifiers
                                                )
import           System.Directory               ( createDirectoryIfMissing )
import           Types.Equity                   ( Equity
                                                , equityClose
                                                , equityDate
                                                , equityTitle
                                                , equityTrades
                                                )

equityToChartable :: [Equity] -> [(Day, Double, Int)]
equityToChartable =
  fmap
      (\e ->
        ( fromJust (equityDate e)
        , fromJust (equityClose e)
        , fromJust (equityTrades e)
        )
      )
    . filter
        (\e -> isJust (equityDate e) && isJust (equityClose e) && isJust
          (equityTrades e)
        )

fillBetween :: String -> [(x1, (y1, y1))] -> EC l2 (PlotFillBetween x1 y1)
fillBetween title vs = liftEC $ do
  plot_fillbetween_title .= title
  color <- takeColor
  plot_fillbetween_style .= solidFillStyle color
  plot_fillbetween_values .= vs

priceColor :: AlphaColour Double
priceColor = opaque $ sRGB 0.25 1 0.25

volumeColor :: AlphaColour Double
volumeColor = opaque $ sRGB 0.5 0.5 1

defColors :: [AlphaColour Double]
defColors = [priceColor, volumeColor]

tidyChartTitle :: [Equity] -> String
tidyChartTitle = filter (/= ' ') . fromMaybe "Unavailable" . equityTitle . head

defChartTitle :: [Equity] -> String
defChartTitle = const "Closing Prices (on left) and Volume (on right)"

tidyChartSVGFilePath :: [Equity] -> String
tidyChartSVGFilePath = ("charts/" ++) . (++ ".svg") . tidyChartTitle

getChartableClosingPrices :: String -> [Equity] -> EC l (PlotLines Day Double)
getChartableClosingPrices lineTitle equityList =
  line lineTitle [[ (d, c) | (d, c, _) <- equityToChartable equityList ]]

getChartableDailyVolmes :: String -> [Equity] -> EC l2 (PlotFillBetween Day Int)
getChartableDailyVolmes lineTitle equityList = fillBetween
  lineTitle
  [ (d, (0, v)) | (d, _, v) <- equityToChartable equityList ]

chartEquity :: [Equity] -> IO ()
chartEquity equityList = do
  let chartTitle     = tidyChartTitle equityList
      chartLineTitle = defChartTitle equityList
      chartFileName  = tidyChartSVGFilePath equityList
      closingPrices  = getChartableClosingPrices chartTitle equityList
      dailyVolumes   = getChartableDailyVolmes "Volume" equityList

  toFile def chartFileName $ do
    setColors defColors
    layoutlr_title .= chartLineTitle
    plotLeft closingPrices
    plotRight dailyVolumes

chartEquities
  :: ( PersistQueryRead backend
     , MonadIO m
     , MonadReader MomentumConfig m
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     , BackendCompatible SqlBackend backend
     )
  => ReaderT backend m ()
chartEquities = do
  activeIdentifiers <- getListOfActiveEquityIdentifiers
  chunk             <- lift $ asks toParallelProcess

  liftIO $ createDirectoryIfMissing True "charts"

  let identifiersChunk = chunksOf chunk activeIdentifiers

  forM_
    identifiersChunk
    (\eachChunk -> do
      listOfEquities <- getListOfEquityFromEquityIdentifiers eachChunk
      let thresheldEquityLists = take 90 <$> listOfEquities
      liftIO $ forM_ thresheldEquityLists chartEquity
    )
