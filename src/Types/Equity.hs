{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

-- |
module Types.Equity where

import           Control.DeepSeq                ( NFData )
import           Data.Aeson                     ( FromJSON
                                                , ToJSON
                                                )
import           Data.Csv                       ( FromRecord
                                                , (.!)
                                                , parseRecord
                                                )
import           Data.Time                      ( Day )
import           Database.Persist.TH            ( mkMigrate
                                                , mkPersist
                                                , persistLowerCase
                                                , share
                                                , sqlSettings
                                                )
import           GHC.Generics                   ( Generic )

-- | Equity's trading day.
type Date = Day

-- | Identifier (i.e. exchange code, unique id, etc.)
type Identifier = String

-- | Equity name/title.
type Title = String

-- | Equity/Stock group.
type Group = String

-- | Equity type. (A, B, etc.)
type Type = String

-- | The price at which the security first trades on a given trading day.
type Open = Double

-- | The highest intra-day price of a stock.
type High = Double

-- | The lowest intra-day price of a stock.
type Low = Double

-- | The final price at which a security is traded on a given trading day.
type Close = Double

-- | The last trade price of the stock.
type Last = Double

-- | The closing price of the stock for the previous trading day.
type PrevClose = Double

-- | The total number of trades of a scrip.
type Trades = Int

-- | The total number of shares transacted of a scrip.
type Shares = Int

-- | Total turnover of a scrip.
type NetTurnOv = Double

-- | Indicator for Corporate Actions like Bonus, Rights, Stock Split,
--   Dividend, Scheme of Arrangement on the day when the same is effective.
type Actions = String

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|
-- | Record to store Equity information.
Equity
    date       Day          Maybe
    identifier Identifier   Maybe
    title      Title        Maybe
    group      Group        Maybe
    type       Type         Maybe
    open       Open         Maybe
    high       High         Maybe
    low        Low          Maybe
    close      Close        Maybe
    last       Last         Maybe
    prevClose  PrevClose    Maybe
    trades     Trades       Maybe
    shares     Shares       Maybe
    netTurnOv  NetTurnOv    Maybe
    actions    Actions      Maybe

    deriving Show Generic
|]

instance NFData Equity

instance ToJSON Equity

instance FromJSON Equity

instance FromRecord Equity where
  parseRecord v = do
    equityIdentifier <- v .! 0
    equityTitle      <- v .! 1
    equityGroup      <- v .! 2
    equityType       <- v .! 3
    equityOpen       <- v .! 4
    equityHigh       <- v .! 5
    equityLow        <- v .! 6
    equityClose      <- v .! 7
    equityLast       <- v .! 8
    equityPrevClose  <- v .! 9
    equityTrades     <- v .! 10
    equityShares     <- v .! 11
    equityNetTurnOv  <- v .! 12
    equityActions    <- v .! 13
    let equityDate = Nothing
    return Equity { .. }
