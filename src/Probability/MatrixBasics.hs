
import Data.Packed.Matrix
import Numeric.LinearAlgebra
import Numeric.LinearAlgebra.Util

mA :: Matrix Double
mA = (2 >< 2) [1.0, 2.0, -2.0, 1.0]

mB :: Matrix Double
mB = (2 >< 2) [0.0, 1.0, 1.0, 0.0]

main :: IO ()
main = do
    print "Matrix A"
    print mA
    print "Matrix B"
    print mB
    print "A + B"
    print (mA + mB)
    print "A x B"
    print (mA <> mB)
    print "A * B"
    print (mA * mB)
    print "B x A"
    print (mB <> mA)
    let c = mA <\> mB
    print "C = A \\ B"
    print c
    disp 2 c
    print "Check A x C = B"
    disp 2 (mA <> c)
    return ()