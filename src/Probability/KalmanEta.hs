import KalmanFilter

estimate :: System -> Kalman -> [SystemObservation] -> Kalman
estimate system = foldl singlestep
    where
        singlestep k =  update system (predict system k)