-- |
module Benchmark.RegressionComparision where

import           Control.Applicative            ( liftA2 )
import           Process.Confidence.LinearRegression
                                                ( generateConfidence
                                                , confidenceWeights
                                                )
import           Types.Confidence               ( ConfidenceRoR
                                                , confidenceHistoricRor
                                                , confidenceYearRor
                                                , confidenceMonthRor
                                                , confidenceWeightedRor
                                                )
import           Types.Equity                   ( Equity
                                                , equityClose
                                                )
import           Utility.Numeral                ( getRateOfReturn )

-- | Weighs direction strength and assigns a value to it.
checkDirectionBenchmark :: Maybe ConfidenceRoR -> Maybe ConfidenceRoR -> Int
checkDirectionBenchmark prediction actual
  | actual > pure 0 && prediction > pure 0 && actual > prediction = 2
  | actual > pure 0 && prediction > pure 0 && actual < prediction = 1
  | otherwise = 0

-- | A helper function that applies 'checkDirection' to each RoR i.e. Historic, Year and Month.
checkDirectionRoRBenchmark
  :: ( Maybe ConfidenceRoR
     , Maybe ConfidenceRoR
     , Maybe ConfidenceRoR
     , Maybe ConfidenceRoR
     )
  -> Maybe ConfidenceRoR
  -> (Int, Int, Int, Int)
checkDirectionRoRBenchmark (preH, preY, preM, preW) act =
  ( checkDirectionBenchmark preH act
  , checkDirectionBenchmark preY act
  , checkDirectionBenchmark preM act
  , checkDirectionBenchmark preW act
  )

-- | This function compares three regression models
-- Historic
-- Year
-- Month
-- and finds which one is closest to the prediction and outputs the weight for each prediction value
-- using weightage.
confidenceWeightsBenchmark
  :: [Equity] -- ^ List of 'Equity'.
  -> (Int, Int, Int, Int) -- ^ Weights for prediction
confidenceWeightsBenchmark equityList = foldr1
  (\(a1, a2, a3, a4) (b1, b2, b3, b4) -> (a1 + b1, a2 + b2, a3 + b3, a4 + b4))
  weightage
 where
  thirtyDaysList       = flip drop equityList <$> [30 .. 60]
-- Because generateConfidence predicts 30 days in the future,
-- dropping first 30 days would mean the latest trading day 'Equity'
-- is the actual value that had to be predicted.
  thirtyDaysConfidence = generateConfidence <$> thirtyDaysList
-- Each 'Confidence' in this list tries to predict 30 days forward.
  predictedRoR =
    (\c ->
        ( confidenceHistoricRor c
        , confidenceYearRor c
        , confidenceMonthRor c
        , confidenceWeightedRor c
        )
      )
      <$> thirtyDaysConfidence
-- This is a list of predicted RoRs.
  actualRoR = zipWith
    (\c p -> liftA2 getRateOfReturn (equityClose p) (equityClose c))
    equityList
    (drop 30 equityList)
-- And this is a list of actual RoRs.
  weightage = zipWith checkDirectionRoRBenchmark predictedRoR actualRoR

multipleConfidenceWeights :: [[Equity]] -> (Int, Int, Int, Int)
multipleConfidenceWeights =
  foldr1
      (\(a1, a2, a3, a4) (b1, b2, b3, b4) ->
        (a1 + b1, a2 + b2, a3 + b3, a4 + b4)
      )
    . fmap confidenceWeightsBenchmark
