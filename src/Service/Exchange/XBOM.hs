{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}

-- |
module Service.Exchange.XBOM where

import           Control.Monad                  ( forM_ )
import           Control.Monad.Except           ( ExceptT )
import           Control.Monad.IO.Class         ( MonadIO )
import           Control.Monad.Reader           ( MonadReader
                                                , ReaderT
                                                , asks
                                                , lift
                                                )
import           Data.List.Split                ( chunksOf )
import           Database.Persist.Sql           ( SqlBackend )
import           Process.Confidence.LinearRegression
                                                ( generateConfidence )
import           Remove.Confidence              ( clearConfidenceTable )
import           Retrive.Equity.Database        ( getListOfActiveEquityIdentifiers
                                                , getListOfEquityFromEquityIdentifiers
                                                )
import           Retrive.Equity.Exchange.XBOM   ( getListOfEquityFromXBOM )
import           Store.Confidence               ( storeManyConfidence )
import           Store.Equity                   ( storeManyEquity )
import           Types.Momentum                 ( MomentumConfig
                                                , MomentumError
                                                , toParallelProcess
                                                )

-- | 'SqlBackend' service runner for 'XBOM' exchange
-- constrained on 'MomentumConfig' with 'MomentumError' effect.
serviceXBOM
  :: (MonadReader MomentumConfig m, MonadIO m)
  => ReaderT SqlBackend (ExceptT MomentumError m) ()
serviceXBOM = do

  storeManyEquity =<< lift getListOfEquityFromXBOM
  -- Stores list of 'Equity' downloaded into database.
  clearConfidenceTable
  -- Clears up 'Confidence' table.
  listOfActiveEquityIdentifiers <- getListOfActiveEquityIdentifiers
  -- Retrives a list of all the active 'Equity' 'Identifier's.
  chunk                         <- lift $ asks toParallelProcess
  -- Retrives chunk. Number of 'Equity' to retrive and generate 'Confidence' for.
  -- Each chunk will get its own seperate connection to the database.
  -- Postgres can only handle 50 connection simultaniously without a connection redirector.
  let identifiersChunk = chunksOf chunk listOfActiveEquityIdentifiers
  -- This variable stores 'Identifier' in a chunk of whatever 'toParallelProcess' is set to.
  forM_
    identifiersChunk
    (\eachChunk -> do
      listOfEquities <- getListOfEquityFromEquityIdentifiers eachChunk
   -- For each chunk of 'Identifier's, 'getListOfEquityFromEquityIdentifiers' will retrive list of 'Equity'.
   -- 'getListOfEquityFromEquityIdentifiers' internally uses
   --
   -- @
   -- [Asc EquityIdentifier, Desc EquityDate]
   -- @
   --
   -- So each [Equity] will be sorted with EquityDate in descending order.
   -- 'Asc' on 'EquityIdentifier' is for 'groupOn' function used
   -- by 'getListOfEquityFromEquityIdentifiers' internally and should have no affect on each chunk.
      let listOfConfidence = generateConfidence <$> listOfEquities
     -- This variable will store list of 'Confidence'.
      storeManyConfidence listOfConfidence
    )
  -- The reason for this 'forM_' loop is to evaluate each chunk strictly.
  -- Otherwise the memory usage can cause unnecessary interupts.
