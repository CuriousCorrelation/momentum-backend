module Autoregression
    ( autoregression
    ) where

import Data.Complex
import qualified Data.Vector.Unboxed as V
import Statistics.Transform (fft, ifft)

autoregression :: V.Vector Double -> V.Vector Double
autoregression = toDouble . ifft . V.map conj . fft . toComplex
    where
        conj (x:+y)     = (x*x + y*y) :+ 0.0
        toDouble        = V.map realPart
        toComplex       = V.map (:+ 0.0)