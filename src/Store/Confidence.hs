{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}

-- |

module Store.Confidence where

import           Database.Persist.Sql           ( BaseBackend
                                                , PersistEntityBackend
                                                , PersistStoreWrite
                                                , insertMany_
                                                , PersistEntity
                                                )
import           Types.Confidence               ( Confidence )
import           Control.Monad.IO.Class         ( MonadIO )
import           Control.Monad.Reader           ( ReaderT )

-- | Synonym for 'insertMany_' constrained on 'Confidence'.
storeManyConfidence
  :: ( MonadIO m
     , PersistStoreWrite backend
     , PersistEntity Confidence
     , PersistEntityBackend Confidence ~ BaseBackend backend
     )
  => [Confidence]
  -> ReaderT backend m ()
storeManyConfidence = insertMany_
