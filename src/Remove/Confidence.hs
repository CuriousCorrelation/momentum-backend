{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

-- |

module Remove.Confidence where

import           Database.Persist.Sql           ( BackendCompatible
                                                , SqlBackend
                                                , rawExecute
                                                )
import           Control.Monad.IO.Class         ( MonadIO )
import           Control.Monad.Reader           ( ReaderT )

clearConfidenceTable
  :: (MonadIO m, BackendCompatible SqlBackend backend) => ReaderT backend m ()
clearConfidenceTable = rawExecute "DELETE FROM confidence" []
