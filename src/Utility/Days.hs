-- |

module Utility.Days where

import           Data.List                      ( (\\) )
import           Data.Maybe                     ( catMaybes )
import           Data.Time                      ( Day
                                                , addDays
                                                , getCurrentTime
                                                , utctDay
                                                )

-- | Given two list of 'Day' returns unique 'Day's that do not appear in first list but do in second.
-- Synonym for
--
-- @
-- firstList 'Data.List.\\' secondList
-- @
--
getUniqueDays
  :: [Day] -- ^ First list.
  -> [Day] -- ^ Second list.
  -> [Day] -- ^ Unique list.
getUniqueDays alreadyIn toAdd = toAdd \\ alreadyIn

-- | Returns list of 'Day's later than given 'Day'.
getLaterDays
  :: Maybe Day -- ^ 'Day' to compare on.
  -> [Day] -- ^ List of 'Day' to compare to.
  -> [Day] -- ^ List of 'Day' that appear later than 'Day' to compare on.
getLaterDays lastestEntryDay =
  catMaybes . filter (> lastestEntryDay) . fmap pure

-- | Returns a list of past 'n' days from given 'Day'.
--
-- @
-- λ> a <- getCurrentDay
-- λ> a
-- 2020-05-25
-- λ> getLastNDays 4 a
-- [2020-05-25,2020-05-24,2020-05-23,2020-05-22,2020-05-21]
-- λ>
-- @
--
getLastNDays
  :: Integer -- ^ Number of days to generate.
  -> Day -- ^ Day **to** which to generate.
  -> [Day] -- ^ List of 'Day' that start from today to n
getLastNDays n = sequence desc where desc = addDays . negate <$> [0 .. n]

-- | Returns today's day in 'Data.Time.Day' type.
getCurrentDay :: IO Day
getCurrentDay = utctDay <$> getCurrentTime
