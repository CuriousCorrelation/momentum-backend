-- |

module Utility.Grouping where

import Data.List (groupBy)

groupOn :: Eq b => (a -> b) -> [a] -> [[a]]
groupOn f = groupBy ((==) `on2` f)
  where (.*.) `on2` fn = \x -> let fx = fn x in \y -> fx .*. fn y
