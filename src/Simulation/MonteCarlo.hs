{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}

-- |

module Simulation.MonteCarlo where

import           Graphics.Rendering.Chart.Easy
import           Graphics.Rendering.Chart.Backend.Diagrams
import           Data.Time.LocalTime
import           Control.Monad.IO.Class
import           Control.Applicative
import           Control.Monad.Reader
import           Data.Time
import           Database.Persist.Sql
import           Types.Equity
import           Retrive.Equity.Database
import           Data.Maybe
import qualified Data.Vector                   as V
import           Statistics.Autocorrelation
import           Utility.PersistentActions      ( runActionT )
import           Text.Pretty.Simple
import           Data.List
import           Statistics.Autocorrelation
import           Charting.Easier

equityAutoCorrelationClosingPriceCharter
  :: ( PersistQueryRead backend
     , MonadIO m
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     )
  => ReaderT backend m ()
equityAutoCorrelationClosingPriceCharter = do
  equityList <- concat <$> getListOfEquityFromEquityIdentifiers [Just "500010"]

  let equityCloses = equityClose <$> equityList
      (Just (autoCorrL, autoCorrM, autoCorrU)) =
        autocorrelation . V.fromList <$> sequence equityCloses

  liftIO $ chartAutoCorrelation
    (V.toList autoCorrL, V.toList autoCorrM, V.toList autoCorrU)

equityAutoCorrelationClosingPriceDiffCharter
  :: ( PersistQueryRead backend
     , MonadIO m
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     )
  => ReaderT backend m ()
equityAutoCorrelationClosingPriceDiffCharter = do
  equityList <- concat <$> getListOfEquityFromEquityIdentifiers [Just "500010"]

  let equityCloses =
        (\e -> liftA2 (-) (equityClose e) (equityPrevClose e)) <$> equityList
      (Just (autoCorrL, autoCorrM, autoCorrU)) =
        autocorrelation . V.fromList <$> sequence equityCloses

  liftIO $ chartAutoCorrelation
    (V.toList autoCorrL, V.toList autoCorrM, V.toList autoCorrU)

deviation
  :: ( PersistQueryRead backend
     , MonadIO m
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     )
  => ReaderT backend m ()
deviation = equityAutoCorrelationClosingPriceCharter
