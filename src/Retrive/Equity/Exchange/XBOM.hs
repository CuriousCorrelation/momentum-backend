{-# LANGUAGE FlexibleContexts #-}

-- |
module Retrive.Equity.Exchange.XBOM
  ( getListOfEquityFromXBOM
  )
where

import           Control.Monad.Except           ( ExceptT )
import           Control.Monad.IO.Class         ( MonadIO
                                                , liftIO
                                                )
import           Control.Monad.Reader           ( MonadReader
                                                , asks
                                                )
import           Internal.Retrive.Equity.Exchange.XBOM
                                                ( eitherToExceptT
                                                , fetchXBOMEquityData
                                                , parseXBOMEquity
                                                )
import           Types.Equity                   ( Equity )
import           Types.Momentum                 ( MomentumConfig(downloadDays)
                                                , MomentumError
                                                , MomentumError
                                                )

-- | Retrives list of 'Equity's from 'XBOM'.
-- Country - India
-- Exchange - XBOM LTD
getListOfEquityFromXBOM
  :: (MonadIO m, MonadReader MomentumConfig m)
  => ExceptT MomentumError m [Equity]
getListOfEquityFromXBOM = do
  days <- asks downloadDays
  eitherToExceptT =<< liftIO
    ((fmap . fmap) (uncurry parseXBOMEquity) (fetchXBOMEquityData days))
