{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}

-- |

module Retrive.Equity.Database where

import           Control.Monad.IO.Class         ( MonadIO
                                                , liftIO
                                                )
import           Control.Monad.Reader           ( ReaderT
                                                , mapReaderT
                                                )
import           Data.Time                      ( Day )
import           Data.Time                      ( addDays )
import           Database.Persist.Sql           ( BackendCompatible
                                                , BaseBackend
                                                , Entity
                                                , Filter
                                                , PersistEntity
                                                , PersistEntityBackend
                                                , PersistQueryRead
                                                , PersistValue(PersistDay)
                                                , SelectOpt(Asc, Desc)
                                                , SqlBackend
                                                , (<-.)
                                                , entityVal
                                                , rawSql
                                                , selectList
                                                , unSingle
                                                )
import           Types.Equity                   ( EntityField
                                                  ( EquityDate
                                                  , EquityIdentifier
                                                  )
                                                , Equity
                                                , Identifier
                                                , equityIdentifier
                                                )
import           Utility.Days                   ( getCurrentDay )
import           Utility.Grouping               ( groupOn )

-- | Synonym for 'selectList' constrained on 'Equity'.
getGeneral
  :: ( PersistQueryRead backend
     , MonadIO m
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     )
  => [Filter Equity]
  -> [SelectOpt Equity]
  -> ReaderT backend m [Entity Equity]
getGeneral = selectList

-- | Synonym for 'entityVal' applied on result of 'getGeneral'.
getGeneralValue
  :: ( PersistQueryRead backend
     , MonadIO m
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     )
  => [Filter Equity]
  -> [SelectOpt Equity]
  -> ReaderT backend m [Equity]
getGeneralValue filterOpts selectOpts =
  mapReaderT ((fmap . fmap) entityVal) $ getGeneral filterOpts selectOpts

-- | Returns a list of all available active (in last 15 days) 'Equity' 'Identifier'.
getListOfActiveEquityIdentifiers
  :: ( PersistQueryRead backend
     , MonadIO m
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     , BackendCompatible SqlBackend backend
     )
  => ReaderT backend m [Maybe Identifier]
getListOfActiveEquityIdentifiers = do
  day15Ago <- liftIO (addDays (-15) <$> getCurrentDay)
  -- Gets the date of 15 days ago.
  mapReaderT ((fmap . fmap) unSingle) $ rawSql
    "SELECT DISTINCT identifier FROM equity WHERE date >= (?)"
    [PersistDay day15Ago]

-- | Return the latest 'Equity' entry date.
getLatestEquityEntryDate
  :: ( PersistQueryRead backend
     , MonadIO m
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     , BackendCompatible SqlBackend backend
     )
  => ReaderT backend m (Maybe Day)
getLatestEquityEntryDate = mapReaderT (fmap (head . fmap unSingle))
  $ rawSql "SELECT date FROM equity ORDER BY date DESC LIMIT 1" []

-- | Returns a list of all available high volume (>= 5000) active (in last 15 days) 'Equity' 'Identifier'.
getListOfHighVolumeActiveEquityIdentifiers
  :: ( PersistQueryRead backend
     , MonadIO m
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     , BackendCompatible SqlBackend backend
     )
  => ReaderT backend m [Maybe Identifier]
getListOfHighVolumeActiveEquityIdentifiers = do
  day15Ago <- liftIO (addDays (-15) <$> getCurrentDay)
  -- Gets the date of 15 days ago.
  mapReaderT ((fmap . fmap) unSingle) $ rawSql
    "SELECT DISTINCT identifier FROM equity WHERE date >= (?) AND trades >= 5000"
    [PersistDay day15Ago]

-- | Returns a list of 'Equity' from given list of 'Identifier'
-- sorted by 'EquityDate' in 'Desc' order and 'Asc' on 'Identifier'.
getListOfEquityFromEquityIdentifiers
  :: ( PersistQueryRead backend
     , MonadIO m
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     )
  => [Maybe Identifier]
  -> ReaderT backend m [[Equity]]
getListOfEquityFromEquityIdentifiers eids =
  mapReaderT (fmap (groupOn equityIdentifier)) $ getGeneralValue
    [EquityIdentifier <-. eids]
    [Asc EquityIdentifier, Desc EquityDate]
    -- Sorted on 'EquityDate' in 'Desc'ending order because 'linearRegression' function in generate confidence
    -- needs value from oldest to latest for prediction.
    --
    -- 'getGeneralValue' sorted on 'EquityDate' in 'Desc'ending order will
    -- alternate each 'Equity' between different 'EquityIdentifier'.
    --
    -- @
    -- getGeneralValue [] [Desc EquityDate]
    -- @
    --
    -- will return results along the line of
    --
    -- @
    -- EquityIdentifier 1 EquityDate 14
    -- EquityIdentifier 2 EquityDate 14
    -- EquityIdentifier 3 EquityDate 14
    -- ...
    -- ...
    -- EquityIdentifier n EquityDate 14
    -- @
    --
    -- But for each 'Equity' to have its own seperate list
    -- (using a function like 'group' compared on 'EquityIdentifier')
    -- the result will need to be like this
    --
    -- @
    -- EquityIdentifier 1 EquityDate 14
    -- EquityIdentifier 1 EquityDate 15
    -- EquityIdentifier 1 EquityDate 16
    -- ...
    -- EquityIdentifier 1 EquityDate N
    -- EquityIdentifier 2 EquityDate 14
    --
    -- EquityIdentifier 2 EquityDate 15
    -- EquityIdentifier 2 EquityDate 16
    -- ...
    -- EquityIdentifier 2 EquityDate N
    -- ...
    -- EquityIdentifier n EquityDate 15
    -- EquityIdentifier n EquityDate 16
    -- ...
    -- EquityIdentifier n EquityDate N
    -- @
    --
    -- which can be achived by sorting first on 'EquityIdentifier' and then on 'EquityDate' in 'Desc'ending order.
