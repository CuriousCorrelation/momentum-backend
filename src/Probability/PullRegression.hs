{-# LANGUAGE OverloadedStrings, TypeFamilies, GADTs, FlexibleContexts #-}
module Main where

import Database.Persist
import Database.Persist.MySQL
import Data.Time
import Control.Monad.Trans.Resource (runResourceT)
import Control.Monad.Logger (runStderrLoggingT)
import Control.Monad.IO.Class (liftIO)

import LegacyTable
import qualified  Data.Vector.Unboxed as U


import Statistics.Sample (mean)
import qualified Data.Vector.Generic as G

-- | Compute the autocovariance of a sample, i.e. the covariance of
-- the sample against a shifted version of itself.
autocovariance :: (G.Vector v Double, G.Vector v Int) => v Double -> v Double
autocovariance a = G.map f . G.enumFromTo 0 $ l-2
  where
    f k = G.sum (G.zipWith (*) (G.take (l-k) c) (G.slice k (l-k) c))
          / fromIntegral l
    c   = G.map (subtract (mean a)) a
    ll  = G.length a
    l   = if ll > 100 then 100 else ll

-- | Compute the autocorrelation function of a sample, and the upper
-- and lower bounds of confidence intervals for each element.
--
-- /Note/: The calculation of the 95% confidence interval assumes a
-- stationary Gaussian process.
autocorrelation :: (G.Vector v Double, G.Vector v Int) => v Double -> (v Double, v Double, v Double)
autocorrelation a = (r, ci (-), ci (+))
  where
    r           = G.map (/ G.head c) c
      where c   = autocovariance a
    dllse       = G.map f . G.scanl1 (+) . G.map square $ r
      where f v = 1.96 * sqrt ((v * 2 + 1) / l)
    l           = fromIntegral (G.length a)
    ci f        = G.cons 1 . G.tail . G.map (f (-1/l)) $ dllse
    square x    = x * x


mysqlConn :: ConnectInfo
mysqlConn = defaultConnectInfo { 
    connectPassword = "arimasen", 
    connectDatabase = "pricer" }

day :: Day
day = fromGregorian 2013 06 25

prettyPrint :: [(Int, Double)] -> IO ()
prettyPrint = mapM_ (\(a,b) -> do 
    putStr $ show a
    putChar ','
    print b)

mkTimeInt :: TimeOfDay -> Int
mkTimeInt = (*) 1000 . truncate . timeOfDayToTime

mkInt :: Tick -> Double
mkInt (Tick _ td ms _ _ _ _) = 
    fromIntegral $ (ms `div` 1000) + mkTimeInt td

durations :: Num a => [a] -> [a]
durations xs = map (\(a,b) -> b - a) tuples
    where
        tuples = zip xs (tail xs)

collectStats :: [Tick] -> [(Int, Double)]
collectStats ticks =  zip [0.. (len - 1)] listResult
    where 
        len            = length listResult
        listResult     = U.toList result
        (result, _, _) = autocorrelation durs
        durs = (U.fromList . durations . map mkInt) ticks

main :: IO ()
main = withMySQLConn mysqlConn $ \conn ->
    runResourceT $ runStderrLoggingT $ flip runSqlConn conn $ do
        printMigration migrateAll
        runMigration migrateAll
        ticks <- selectList [TickDay ==. day] [Asc TickTime, Asc TickMksec]
        let stats = collectStats $ map entityVal ticks
        liftIO $ prettyPrint stats
        return ()