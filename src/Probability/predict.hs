import KalmanFilter
import ToSystem

predict' :: ToSystem a => a -> Kalman -> Kalman
predict' = predict . toSystem

update' :: ToSystem a => a -> Kalman -> SystemObservation -> Kalman
update' = update . toSystem