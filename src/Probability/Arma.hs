module Arma
    ( ARMA(..)
    ) where

import Numeric.Container
import ToSystem
import KalmanFilter

data ARMA = ARMA {
    arTerms :: Vector Double,
    maTerms :: Vector Double
} deriving (Show)

p :: ARMA -> Int
p = dim . arTerms

q :: ARMA -> Int
q = dim . maTerms

instance ToSystem ARMA where
     toObservationModel a = ObservationModel 
        (build (n,1) builder) 
        (konst 0.0 (1, n))
        where 
            n              = (1 + p a) :: Int
            builder i _    = if i == 0 then 1.0 else 0.0

     toSystemModel a@(ARMA ar ma) = SystemModel
        (buildMatrix n n transitions) 
        (buildMatrix 1 n covariance)
        where
            np              = (1 + p a) :: Int
            nq              = (1 + q a) :: Int
            n               = max np nq
            transitions (i, j) 
                | i == j && i < n-1 = ma @> i
                | otherwise = 0.0
            covariance (_, j)
                | j == 0    = 1.0
                | otherwise = ar @> (j-1)