{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}

-- |

module Store.Equity where

import           Database.Persist.Sql           ( BaseBackend
                                                , PersistEntityBackend
                                                , PersistStoreWrite
                                                , insertMany_
                                                , PersistEntity
                                                )
import           Types.Equity                   ( Equity )
import           Control.Monad.IO.Class         ( MonadIO )
import           Control.Monad.Reader           ( ReaderT )

-- | Synonym for 'insertMany_' constrained on 'Equity'.
storeManyEquity
  :: ( MonadIO m
     , PersistStoreWrite backend
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     )
  => [Equity]
  -> ReaderT backend m ()
storeManyEquity = insertMany_
