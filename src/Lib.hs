module Lib
  ( someFunc
  )
where

import           CLI.Momentum                   ( momentumCLIParserExecutor )

someFunc :: IO ()
someFunc = momentumCLIParserExecutor
