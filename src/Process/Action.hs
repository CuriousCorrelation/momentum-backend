-- |
module Process.Action where

import           Control.Monad.IO.Unlift        ( MonadUnliftIO )
import           Control.Monad.Logger           ( LoggingT
                                                , runStdoutLoggingT
                                                )
import           Control.Monad.Reader           ( ReaderT
                                                , runReaderT
                                                )
import           Database.Persist.Postgresql    ( ConnectionString
                                                , SqlBackend
                                                , withPostgresqlConn
                                                )

-- | Runs a simple persistent postgres ReaderT action constrained on 'SqlBackend'.
-- Uses a simple postgres connection string.
runAction
  :: MonadUnliftIO m
  => ConnectionString
  -> ReaderT SqlBackend (LoggingT m) a
  -> m a
runAction connStr action =
  runStdoutLoggingT $ withPostgresqlConn connStr $ \backend ->
    runReaderT action backend
