-- |

module Process.Confidence.LinearRegression
  ( generateConfidence
  , getRateOfReturn
  , confidenceWeights
  )
where

import           Control.Monad                  ( zipWithM )
import           Data.Bifunctor                 ( bimap )
import           Control.Applicative            ( liftA2 )
import qualified Data.Vector                   as V
import           Types.Confidence               ( Confidence(..)
                                                , ConfidenceRoR
                                                )
import           Types.Equity                   ( Equity
                                                , equityClose
                                                , equityDate
                                                , equityIdentifier
                                                , equityShares
                                                , equityTitle
                                                , equityTrades
                                                )
import           Utility.Numeral                ( estimation
                                                , getAverage
                                                , linearRegression'
                                                , getRateOfReturn
                                                )

baseLine :: Int -> [Maybe Double]
baseLine len = pure . fromIntegral <$> [len, len - 1 .. 1]

unzipBimapSeq
  :: (Traversable t, Monad m)
  => ([a1] -> t (m a2))
  -> [(a1, a1)]
  -> (m (t a2), m (t a2))
unzipBimapSeq fn = bimap (sequence . fn) (sequence . fn) . unzip

-- | Weighs direction strength and assigns a value to it.
checkDirection :: Maybe ConfidenceRoR -> Maybe ConfidenceRoR -> Int
checkDirection prediction actual
  | actual > pure 0 && prediction > pure 0 && actual > prediction = 2
  | actual > pure 0 && prediction > pure 0 && actual < prediction = 1
  | otherwise = 0

-- | A helper function that applies 'checkDirection' to each RoR i.e. Historic, Year and Month.
checkDirectionRoR
  :: (Maybe ConfidenceRoR, Maybe ConfidenceRoR, Maybe ConfidenceRoR)
  -> Maybe ConfidenceRoR
  -> (Int, Int, Int)
checkDirectionRoR (preH, preY, preM) act =
  (checkDirection preH act, checkDirection preY act, checkDirection preM act)

-- | This function compares three regression models
-- Historic
-- Year
-- Month
-- and finds which one is closest to the prediction and outputs the weight for each prediction value
-- using weightage.
confidenceWeights
  :: [Equity] -- ^ List of 'Equity'.
  -> (Int, Int, Int) -- ^ Weights for prediction
confidenceWeights []         = (1, 1, 1)
confidenceWeights equityList = if null weightage
  then (1, 1, 1)
  else foldr1 (\(a1, a2, a3) (b1, b2, b3) -> (a1 + b1, a2 + b2, a3 + b3))
              weightage
 where
  thirtyDaysList       = flip drop equityList <$> [30 .. 60]
  -- Because generateConfidence predicts 30 days in the future,
  -- dropping first 30 days would mean the latest trading day 'Equity'
  -- is the actual value that had to be predicted.
  thirtyDaysConfidence = generateConfidence <$> thirtyDaysList
  -- Each 'Confidence' in this list tries to predict 30 days forward.
  predictedRoR =
    (\c -> (confidenceHistoricRor c, confidenceYearRor c, confidenceMonthRor c))
      <$> thirtyDaysConfidence
  -- This is a list of predicted RoRs.
  actualRoR = zipWith
    (\c p -> liftA2 getRateOfReturn (equityClose p) (equityClose c))
    equityList
    (drop 30 equityList)
  -- And this is a list of actual RoRs.
  weightage = zipWith checkDirectionRoR predictedRoR actualRoR

-- | Returns 'Confidence' from list of 'Equity' sorted of 'date' desc.
generateConfidence :: [Equity] -> Confidence
generateConfidence equityList = Confidence latestClose
                                           historicClosesAvg
                                           yearClosesAvg
                                           monthClosesAvg
                                           historicPredRoR
                                           yearPredRoR
                                           monthPredRoR
                                           weightedRoR
                                           availableFrom
                                           availableTo
                                           identifier
                                           title
                                           trades
                                           shares

 where
  latestEquity        = head equityList
  earliestEquity      = last equityList

  latestClose         = equityClose latestEquity

  predictFor          = fromIntegral $ length equityList + 30

  historicLogVolumes  = fmap (log . fromIntegral) . equityTrades <$> equityList
  hLogVolumesLen      = length historicLogVolumes

  historicCloses      = equityClose <$> equityList
  historicClosesAvg   = getAverage historicCloses
  yearClosesAvg       = getAverage $ take 365 historicCloses
  monthClosesAvg      = getAverage $ take 30 historicCloses

  -- R referes to the variable being regressable i.e. zipped with base line.
  historicLogVolumesR = zip (baseLine hLogVolumesLen) historicLogVolumes
  yearLogVolumesR     = take 365 historicLogVolumesR
  monthLogVolumesR    = take 30 yearLogVolumesR

  -- V referes to the variable being vectorized i.e. from [Maybe Double] to Vector (Mayeb Double).
  historicLogVolumesV = unzipBimapSeq V.fromList historicLogVolumesR
  yearLogVolumesV     = unzipBimapSeq V.fromList yearLogVolumesR
  monthLogVolumesV    = unzipBimapSeq V.fromList monthLogVolumesR

  -- E referes to the variable being a estimatable. i.e being of type Maybe (alpha, beta).
  historicLogVolumesE = uncurry (liftA2 linearRegression') historicLogVolumesV
  yearLogVolumesE     = uncurry (liftA2 linearRegression') yearLogVolumesV
  monthLogVolumesE    = uncurry (liftA2 linearRegression') monthLogVolumesV

  -- P referes to the variable being the actual prediction.
  historicLogVolumesP = flip estimation predictFor <$> historicLogVolumesE
  yearLogVolumesP     = flip estimation predictFor <$> yearLogVolumesE
  monthLogVolumesP    = flip estimation predictFor <$> monthLogVolumesE

  -- Here CLVR refers to Close * LogVolume.
  historicCLV         = zipWith (liftA2 (*)) historicLogVolumes historicCloses
  hCLVLen             = length historicCLV

  historicCLVR        = zip (baseLine hCLVLen) historicCLV
  yearCLVR            = take 365 historicCLVR
  monthCLVR           = take 30 yearCLVR

  historicCLVV        = unzipBimapSeq V.fromList historicCLVR
  yearCLVV            = unzipBimapSeq V.fromList yearCLVR
  monthCLVV           = unzipBimapSeq V.fromList monthCLVR

  historicCLVE        = uncurry (liftA2 linearRegression') historicCLVV
  yearCLVE            = uncurry (liftA2 linearRegression') yearCLVV
  monthCLVE           = uncurry (liftA2 linearRegression') monthCLVV

  historicCLVP        = flip estimation predictFor <$> historicCLVE
  yearCLVP            = flip estimation predictFor <$> yearCLVE
  monthCLVP           = flip estimation predictFor <$> monthCLVE

  historicPrediction  = liftA2 (/) historicCLVP historicLogVolumesP
  yearPrediction      = liftA2 (/) yearCLVP yearLogVolumesP
  monthPrediction     = liftA2 (/) monthCLVP monthLogVolumesP

  historicPredRoR     = liftA2 getRateOfReturn latestClose historicPrediction
  yearPredRoR         = liftA2 getRateOfReturn latestClose yearPrediction
  monthPredRoR        = liftA2 getRateOfReturn latestClose monthPrediction

  (wH, wY, wM)        = confidenceWeights equityList
  confidenceWeightT   = pure $ fromIntegral $ wH + wY + wM

  weightedRoRT        = sum <$> zipWithM
    (\a b -> (* fromIntegral a) <$> b)
    [wH, wY, wM]
    [historicPredRoR, yearPredRoR, monthPredRoR]

  weightedRoR   = liftA2 (/) weightedRoRT confidenceWeightT

  availableFrom = equityDate earliestEquity
  availableTo   = equityDate latestEquity

  identifier    = equityIdentifier latestEquity
  title         = equityTitle latestEquity
  trades        = equityTrades latestEquity
  shares        = equityShares latestEquity
