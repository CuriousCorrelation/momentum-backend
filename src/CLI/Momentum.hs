-- |
module CLI.Momentum where

import           Options.Applicative            ( Parser
                                                , help
                                                , long
                                                , short
                                                , switch
                                                , helper
                                                , execParser
                                                , info
                                                , fullDesc
                                                , progDesc
                                                , header
                                                , (<**>)
                                                )
import           Types.CLIOptions               ( MomentumCLI(MomentumCLI) )
import           Service.Runner
import           Charting.Equity
import           Defaults.Momentum
import           Service.Runner
import           Benchmark.Confidence
import           Utility.PersistentActions
import           Text.Pretty.Simple
import           Simulation.MonteCarlo

momentumCLIParser :: Parser MomentumCLI
momentumCLIParser =
  MomentumCLI
    <$> switch
          (long "run" <> short 'r' <> help
            "Runs database population and charting."
          )
    <*> switch
          (long "benchmark-lr" <> help "Runs linear regression benchmarking.")
    <*> switch (long "benchmark-w" <> help "Runs weighted benchmarking.")
    <*> switch (long "test" <> short 't' <> help "Runs weighted benchmarking.")

momentumCLIParserExecutor :: IO ()
momentumCLIParserExecutor = runner =<< execParser opts
 where
  opts = info
    (momentumCLIParser <**> helper)
    (fullDesc <> progDesc "Run Momentum server via MomentumCLI" <> header
      "Momentum"
    )

runner :: MomentumCLI -> IO ()
runner (MomentumCLI runPopulation' runLRBenchmark' runWBenchmark' runTest')
  | runPopulation'  = serviceXBOM
  | runLRBenchmark' = benchmarkLR
  | runWBenchmark'  = benchmarkLRW
  | runTest'        = runTest
  | otherwise       = putStrLn "No options provided."

benchmarkLR :: IO ()
benchmarkLR =
  pPrint =<< flip runActionT benchmarkLinearRegression =<< defConnectionString

benchmarkLRW :: IO ()
benchmarkLRW =
  pPrint
    =<< flip runActionT benchmarkLinearRegressionWeightage
    =<< defConnectionString

serviceXBOM :: IO ()
serviceXBOM = do
  runXBOMService =<< defMomentumConfig
  flip runChunkedActionT chartEquities =<< defMomentumConfig

runTest :: IO ()
runTest = flip runActionT deviation =<< defConnectionString
