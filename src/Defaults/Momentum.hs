{-# LANGUAGE OverloadedStrings #-}

-- |
module Defaults.Momentum where

import           Data.Time                      ( Day )
import           Database.Persist.Postgresql    ( ConnectionString )
import           Process.Action                 ( runAction )
import           Retrive.Equity.Database        ( getLatestEquityEntryDate )
import           Types.Momentum                 ( Exchange(XBOM)
                                                , MomentumConfig(MomentumConfig)
                                                , MomentumTestConfig
                                                  ( MomentumTestConfig
                                                  )
                                                )
import           Utility.Days                   ( getCurrentDay
                                                , getLastNDays
                                                )

defConnectionString :: IO ConnectionString
defConnectionString =
  return
    "host=localhost port=5432 user=momentum dbname=equity password=TsPTBmxpd201003dxedeNTUM"

defExchange :: IO Exchange
defExchange = return XBOM

defDownloadToFix :: IO Bool
defDownloadToFix = return False

defDownloadDays :: IO [Day]
defDownloadDays = do
  toFix                 <- defDownloadToFix
  connStr               <- defConnectionString
  latestEquityEntryDate <- runAction connStr getLatestEquityEntryDate
  lastNDays             <- getLastNDays 30 <$> getCurrentDay

  if toFix
    then return lastNDays
    else return $ filter (\a -> pure a > latestEquityEntryDate) lastNDays

defToParallelProcess :: IO Int
defToParallelProcess = return 10

defMomentumConfig :: IO MomentumConfig
defMomentumConfig =
  MomentumConfig
    <$> defDownloadDays
    <*> defExchange
    <*> defDownloadToFix
    <*> defConnectionString
    <*> defToParallelProcess

defTestEquityThreshold :: IO Int
defTestEquityThreshold = return 200

defTestConnectionString :: IO ConnectionString
defTestConnectionString =
  return
    "host=192.168.0.119 port=5432 user=momentum dbname=equity password=TsPTBmxpd201003dxedeNTUM"

defTestChunks :: IO Int
defTestChunks = return 10

defMomentumTestConfig :: IO MomentumTestConfig
defMomentumTestConfig =
  MomentumTestConfig
    <$> defTestEquityThreshold
    <*> defTestConnectionString
    <*> defTestChunks
