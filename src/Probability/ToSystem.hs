module ToSystem (
    ToSystem (..)
    ) where

import KalmanFilter

class ToSystem a where
    toSystemModel :: a -> SystemModel
    toObservationModel :: a -> ObservationModel

    toSystem :: a -> System
    toSystem model = System (toSystemModel model) (toObservationModel model)

instance ToSystem System where
     toSystemModel = sModel
     toObservationModel = sObservation
     toSystem = id