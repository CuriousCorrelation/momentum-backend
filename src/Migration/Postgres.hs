-- |
module Migration.Postgres where

import           Database.Persist.Postgresql    ( ConnectionString
                                                , runMigration
                                                )
import           Database.Persist.Sql           ( Migration )
import           Process.Action                 ( runAction )
import qualified Types.Confidence              as EConfidence
import qualified Types.Equity                  as EEquity

-- | Migrates database.
migrateDB :: ConnectionString -> Migration -> IO ()
migrateDB connectionString migrationInfo =
  runAction connectionString (runMigration migrationInfo)

-- | Migrates 'Equity' database using 'defConnectionString'.
migrateDBEquity :: ConnectionString -> IO ()
migrateDBEquity connStr = runAction connStr (runMigration EEquity.migrateAll)

-- | Migrates 'Confidence' database using 'defConnectionString'.
migrateDBConfidence :: ConnectionString -> IO ()
migrateDBConfidence connStr =
  runAction connStr (runMigration EConfidence.migrateAll)
