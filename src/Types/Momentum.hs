{-# LANGUAGE FlexibleContexts #-}

-- |
module Types.Momentum where

import           Data.ByteString                ( ByteString )
import           Data.Time                      ( Day )

data MomentumError
  = EmptyRequestResponse String
  | SomeError
  | GenericError
  deriving (Show)

data Exchange
  = HSTC -- Country - Vietnam              Exchange - Hanoi Stock Exchange
  | XSTC -- Country - Vietnam              Exchange - Hochiminh Stock Exchange
  | XASE -- Country - USA                  Exchange - NYSE American
  | BATS -- Country - USA                  Exchange - CBOE BATS BZX
  | XNYS -- Country - USA                  Exchange - New York Stock Exchange
  | ARCX -- Country - USA                  Exchange - NYSE Arca
  | XNMS -- Country - USA                  Exchange - NASDAQ Global Market
  | XNCM -- Country - USA                  Exchange - NASDAQ Capital Market
  | OOTC -- Country - USA                  Exchange - OTC markets
  | XNGS -- Country - USA                  Exchange - NASDAQ Global Select
  | XDFM -- Country - United Arab Emirates Exchange - Dubai Financial Market
  | DIFX -- Country - United Arab Emirates Exchange - Nasdaq Dubai
  | XADS -- Country - United Arab Emirates Exchange - Abu Dhabi Securities Exchange
  | XSWX -- Country - Switzerland          Exchange - Six Swiss Exchange
  | XVTX -- Country - Switzerland          Exchange - Six Swiss - Blue Chips Segment
  | XBSE -- Country - Romania              Exchange - Spot Regulated Market - Bvb
  | MISX -- Country - Russia               Exchange - Standard-Classica-Forts
  | XKAR -- Country - Pakistan             Exchange - Karachi Stock Exchange (Guarantee) Limited
  | XKRX -- Country - South Korea          Exchange - Korea Exchange (Stock Market)
  | XKOS -- Country - South Korea          Exchange - Korea Exchange (Kosdaq)
  | XTKS -- Country - Japan                Exchange - Tokyo Stock Exchange
  | XBOM -- Country - India                Exchange - BSE LTD
  | XNSE -- Country - India                Exchange - National Stock Exchange Of India
  | XEQT -- Country - Germany              Exchange - Equiduct Trading
  | XBER -- Country - Germany              Exchange - Boerse Berlin
  | XDUS -- Country - Germany              Exchange - Dusseldorf Stock Exchange
  | XFRA -- Country - Germany              Exchange - Frankfurt Stock Exchange
  | XMUN -- Country - Germany              Exchange - Munich Stock Exchange
  | XSTU -- Country - Germany              Exchange - Stuttgart Stock Exchange
  | XETR -- Country - Germany              Exchange - Deutsche Boerse Xetra
  | XQTX -- Country - Germany              Exchange - QUOTRIX
  | XCAI -- Country - Egypt                Exchange - Egyptian Exchange
  | XZAG -- Country - Croatia              Exchange - Zagreb Stock Exchange
  | XPRA -- Country - Czech Republic       Exchange - Prague Stock Exchange
  | XCNQ -- Country - Canada               Exchange - Canadian Securities Exchange
  | XTSE -- Country - Canada               Exchange - Toronto Stock Exchange
  | XTSX -- Country - Canada               Exchange - TSX Venture Exchange
  | NEOE -- Country - Canada               Exchange - Aequitas NEO Exchange (Lit Book)
  | XSGO -- Country - Chile                Exchange - Santiago Stock Exchange
  | XSHG -- Country - China                Exchange - Shanghai Stock Exchange
  | XSHE -- Country - China                Exchange - Shenzhen Stock Exchange
  | XBOG -- Country - Colombia             Exchange - Bolsa De Valores De Colombia
  | BVMF -- Country - Brazil               Exchange - XBSP
  | XNEC -- Country - Australia            Exchange - National Stock Exchange of Australia
  | XASX -- Country - Australia            Exchange - Asx - All Markets
  | XBUE -- Country - Argentina            Exchange - Bolsa de Comercio de Buenos Aires
  | XWBO -- Country - Austria              Exchange - Wiener Boerse Ag
  | XBAH -- Country - Bahrain              Exchange - Bahrain Bourse
  | XDHA -- Country - Bangladesh           Exchange - Dhaka Stock Exchange Ltd
  | XBRU -- Country - Belgium              Exchange - Nyse Euronext - Euronext Brussels
  | XCYS -- Country - Cyprus               Exchange - Cyprus Stock Exchange
  | XCSE -- Country - Denmark              Exchange - Omx Nordic Exchange Copenhagen A/S
  | XHEL -- Country - Finland              Exchange - Nasdaq Omx Helsinki Ltd.
  | XPAR -- Country - France               Exchange - Euronext Paris
  | XATH -- Country - Greece               Exchange - Athens Exchange S.A. Cash Market
  | XHKG -- Country - Hong Kong            Exchange - Hong Kong Exchange
  | XBUD -- Country - Hungary              Exchange - Budapest Stock Exchange
  | XICE -- Country - Iceland              Exchange - First North Iceland
  | XIDX -- Country - Indonesia            Exchange - Indonesia Stock Exchange
  | XDUB -- Country - Ireland              Exchange - Irish Stock Exchange - All Market
  | XTAE -- Country - Israel               Exchange - Tel Aviv Stock Exchange
  | MTAA -- Country - Italy                Exchange - Borsa Italiana S.P.A.
  | XAMM -- Country - Jordan               Exchange - Amman Stock Exchange
  | XNAI -- Country - Kenya                Exchange - Nairobi Stock Exchange
  | XKUW -- Country - Kuwait               Exchange - Kuwait Stock Exchange
  | XLUX -- Country - Luxembourg           Exchange - Luxembourg Stock Exchange
  | XKLS -- Country - Malaysia             Exchange - Bursa Malaysia
  | XMEX -- Country - Mexico               Exchange - Bolsa Mexicana De Valores (Mexican Stock Exchange)
  | XCAS -- Country - Morocco              Exchange - Casablanca Stock Exchange
  | XNZE -- Country - New Zealand          Exchange - New Zealand Exchange Ltd
  | XNSA -- Country - Nigeria              Exchange - Nigerian Stock Exchange
  | XOSL -- Country - Norway               Exchange - Oslo Bors Asa
  | NOTC -- Country - Norway               Exchange - Norwegian OTC Market
  | XMUS -- Country - Oman                 Exchange - Muscat Securities Market
  | XLIM -- Country - Peru                 Exchange - Bolsa De Valores De Lima
  | XPHS -- Country - Philippines          Exchange - Philippine Stock Exchange Inc.
  | XWAR -- Country - Poland               Exchange - Warsaw Stock Exchange/Equities/Main Market
  | XLIS -- Country - Portugal             Exchange - Nyse Euronext - Euronext Lisbon
  | DSMD -- Country - Qatar                Exchange - Qatar Exchange
  | XSAU -- Country - Saudi Arabia         Exchange - Saudi Stock Exchange
  | XSES -- Country - Singapore            Exchange - Singapore Exchange
  | XLJU -- Country - Slovenia             Exchange - Ljubljana Stock Exchange (Official Market)
  | XJSE -- Country - South Africa         Exchange - Johannesburg Stock Exchange
  | XMAD -- Country - Spain                Exchange - Bolsa De Madrid
  | XCOL -- Country - Sri Lanka            Exchange - Colombo Stock Exchange
  | XNGM -- Country - Sweden               Exchange - Nordic Growth Market
  | XSTO -- Country - Sweden               Exchange - Nasdaq Omx Nordic
  | XDSE -- Country - Syria                Exchange - Damascus Securities Exchange
  | ROCO -- Country - Taiwan               Exchange - Tapei Exchange
  | XTAI -- Country - Taiwan               Exchange - Taiwan Stock Exchange
  | XBKK -- Country - Thailand             Exchange - Stock Exchange Of Thailand
  | TOMX -- Country - The Netherlands      Exchange - TOM MTF
  | XAMS -- Country - The Netherlands      Exchange - Nyse Euronext - Euronext Amsterdam
  | XIST -- Country - Turkey               Exchange - Istanbul Stock Exchange
  | BATE -- Country - United Kingdom       Exchange - BATS Trading Europe
  | CHIX -- Country - United Kingdom       Exchange - Chi-X Eruope
  | XLON -- Country - United Kingdom       Exchange - London Stock Exchange
  | XPOS -- Country - United Kingdom       Exchange - ITG Posit
  | TRQX -- Country - United Kingdom       Exchange - Turquoise
  | BOAT -- Country - United Kingdom       Exchange - Cinnober BOAT

data MomentumConfig =
  MomentumConfig
    { downloadDays         :: [Day]
    , downloadExchange     :: Exchange
    , downloadToFix        :: Bool
    , connectionString     :: ByteString
    , toParallelProcess    :: Int
    }

data MomentumTestConfig =
  MomentumTestConfig
    { testEquityThreshold  :: Int
    , testConnectionString :: ByteString
    , testChunks           :: Int
    }
