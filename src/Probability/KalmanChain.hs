import KalmanFilter

estimate :: System -> Kalman -> [SystemObservation] -> Kalman
estimate system kalman observations = foldl singlestep kalman observations
    where
        singlestep k obs =  update system (predict system k) obs