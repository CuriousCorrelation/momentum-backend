-- |
module Service.Runner
  ( runService
  , runXBOMService
  )
where

import           Control.Monad.Except           ( ExceptT
                                                , runExceptT
                                                )
import           Control.Monad.Logger           ( LoggingT
                                                , runStdoutLoggingT
                                                )
import           Control.Monad.Reader           ( ReaderT
                                                , runReaderT
                                                )
import           Database.Persist.Postgresql    ( SqlBackend
                                                , withPostgresqlConn
                                                )
import           Service.Exchange.XBOM          ( serviceXBOM )
import           Types.Momentum                 ( MomentumConfig
                                                , connectionString
                                                )

-- | Runs an exchange service.
--
-- An exchange service has the type
--
-- @
-- serviceX___
--   :: ( Control.Monad.Reader.Class.MonadReader MomentumConfig m
--      , Control.Monad.IO.Class.MonadIO m
--      )
--   => ReaderT SqlBackend (ExceptT Types.Momentum.MomentumError m) ()
-- @
--
-- For 'persistent''s 'withPostgresqlConn' to work,
-- the 'ExceptT' moand transformer need to be run before parametarizing.
-- So instead of doing
--
-- @
-- λ> :t withPostgresqlConn undefined $ runReaderT serviceX___
-- withPostgresqlConn undefined $ runReaderT serviceX___
--   :: (Control.Monad.IO.Unlift.MonadUnliftIO
--         (ExceptT Types.Momentum.MomentumError m),
--       Control.Monad.Logger.MonadLogger m,
--       Control.Monad.Reader.Class.MonadReader MomentumConfig m,
--       Control.Monad.IO.Class.MonadIO m) =>
--      ExceptT Types.Momentum.MomentumError m ()
-- @
--
-- it needs to be done like this
--
-- @
-- λ> :t withPostgresqlConn undefined $ runExceptT . runReaderT serviceX___
-- withPostgresqlConn undefined $ runExceptT . runReaderT serviceX___
--   :: (Control.Monad.IO.Unlift.MonadUnliftIO m,
--       Control.Monad.Logger.MonadLogger m,
--       Control.Monad.Reader.Class.MonadReader MomentumConfig m) =>
--      m (Either Types.Momentum.MomentumError ())
-- @
--
-- before there cannot be an 'UnliftIO' instance for 'ExceptT'.
--
-- Refer to  <https://stackoverflow.com/questions/62360268/how-to-fix-missing-instance-of-io-for-a-function-constrained-on-monadreader-and this StackOverflow question>
--
runService
  :: Show a
  => MomentumConfig
  -> ReaderT
       SqlBackend
       (ExceptT a (LoggingT (ReaderT MomentumConfig IO)))
       ()
  -> IO ()
runService appConfig service =
  let connStr = connectionString appConfig
  in  either print return
        =<< runReaderT
              ( runStdoutLoggingT
              $ withPostgresqlConn connStr
              $ runExceptT
              . runReaderT service
              )
              appConfig

-- | XBOM service runner.
runXBOMService :: MomentumConfig -> IO ()
runXBOMService appConfig = runService appConfig serviceXBOM
