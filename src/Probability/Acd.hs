module Acd 
    ( Acd (..)
    ) where
    
import ToSystem
import Data.Packed.Vector

data Acd = Acd {
    acdA :: Double,
    acdB :: Vector Double,
    acdC :: Vector Double
} deriving (Show)

instance ToSystem Acd where
    toSystemModel = undefined
    
    toObservationModel = undefined
