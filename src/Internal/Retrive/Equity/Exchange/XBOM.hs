-- |
module Internal.Retrive.Equity.Exchange.XBOM where

import           Codec.Archive.Zip              ( Archive
                                                , Entry
                                                , eRelativePath
                                                , fromEntry
                                                , toArchive
                                                , zEntries
                                                )
import           Control.Concurrent.Async       ( mapConcurrently )
import           Control.Monad.Except           ( ExceptT
                                                , throwError
                                                )
import           Data.ByteString.Lazy           ( ByteString )
import           Data.Char                      ( isNumber )
import           Data.Csv                       ( HasHeader(..)
                                                , decode
                                                )
import           Data.List                      ( intercalate )
import           Data.List.Split                ( chunksOf
                                                , splitOn
                                                )
import           Data.Time                      ( Day
                                                , defaultTimeLocale
                                                , parseTimeM
                                                )
import qualified Data.Vector                   as V
                                                ( Vector
                                                , concat
                                                , toList
                                                )
import           Network.HTTP.Client            ( Manager
                                                , Request
                                                , Response
                                                , httpLbs
                                                , newManager
                                                , parseRequest
                                                , responseBody
                                                , responseStatus
                                                )
import           Network.HTTP.Client.TLS        ( tlsManagerSettings )
import           Network.HTTP.Types.Status      ( statusCode )
import           Types.Equity                   ( Equity
                                                , equityDate
                                                )
import           Types.Momentum                 ( MomentumError
                                                , MomentumError
                                                  ( EmptyRequestResponse
                                                  )
                                                )

-- | Base link for XBOMIndia equity file download.
baseLink :: String
baseLink = "https://www.bseindia.com/download/BhavCopy/Equity/EQ"

-- | Suffix of Equity ZIP file.
linkExtention :: String
linkExtention = "_CSV.ZIP"

-- | Produces a link from a 'Day' type.
--
-- @
-- λ> a <- getCurrentDay
-- λ> a
-- 2020-05-25
-- λ> generateLink a
-- "https://www.bseindia.com/download/BhavCopy/Equity/EQ250520_CSV.ZIP"
-- λ>
-- @
--
generateLink :: Day -> String
generateLink =
  (++ linkExtention)
    . (baseLink ++)
    . concat
    . reverse
    . splitOn "-"
    . drop 2
    . show

-- | Filters successful reponses (code 200) and discards the rest.
filterSuccess :: [Response ByteString] -> [Response ByteString]
filterSuccess = filter ((== 200) . statusCode . responseStatus)

-- | Converts Day and Response for that day into a zip file with its name as the day it was created on.
makeValid :: Response ByteString -> Archive
makeValid = toArchive . responseBody

-- | Processes 'Entry' into 'FilePath' and 'ByteString'.
processEntry :: Entry -> (FilePath, ByteString)
processEntry entry = (eRelativePath entry, fromEntry entry)

-- | Synonym for
--
-- @
-- 'parallel' . 'fmap' 'parseRequest'
-- @
--
parallelProcessURLs :: [String] -> IO [Request]
parallelProcessURLs = mapConcurrently parseRequest

-- | Synonym for
--
-- @
-- 'parallel' . 'fmap' ('`httpLbs`' 'manager')
-- @
--
parallelProcessResponses :: Manager -> [Request] -> IO [Response ByteString]
parallelProcessResponses manager = mapConcurrently (`httpLbs` manager)

-- | Given a list of days returns XBOM Equity information for those days.
fetchXBOMEquityData :: [Day] -> IO [(FilePath, ByteString)]
fetchXBOMEquityData days = do
  manager <- newManager tlsManagerSettings
  let reqs = generateLink <$> days
  requests  <- parallelProcessURLs reqs
  responses <- parallelProcessResponses manager requests
  let validResponses = filterSuccess responses
      validList      = makeValid <$> validResponses
  return $ concat $ fmap processEntry . zEntries <$> validList

-- | Parses 'Day' from 'FilePath'.
--
-- >>> fileNameToDay "04-08-20"
-- Just 2020-08-04
--
-- >>> fileNameToDay "031220"
-- Just 2020-12-03
fileNameToDay :: FilePath -> Maybe Day
fileNameToDay =
  parseTimeM True defaultTimeLocale "%d-%m-%y"
    . intercalate "-"
    . chunksOf 2
    . filter isNumber

-- | Converts empty response error to 'EmptyRequestResponse' and wrapes result in 'ExceptT'.
eitherToExceptT
  :: (Monad m) => [Either String (V.Vector a)] -> ExceptT MomentumError m [a]
eitherToExceptT =
  either (throwError . EmptyRequestResponse) (pure . V.toList . V.concat)
    . sequence

-- | Parses XBOM Equity file from given file title and file contents.
parseXBOMEquity :: String -> ByteString -> Either String (V.Vector Equity)
parseXBOMEquity fileTitle info =
  fmap (\a -> a { equityDate = parsedDay }) <$> content
 where
  content   = decode HasHeader info
  parsedDay = fileNameToDay fileTitle
