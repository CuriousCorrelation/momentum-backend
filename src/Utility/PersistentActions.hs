-- |
module Utility.PersistentActions where

import           Types.Momentum                 ( connectionString
                                                , MomentumConfig
                                                )
import           Control.Monad.IO.Unlift        ( MonadUnliftIO )
import           Control.Monad.Logger           ( LoggingT
                                                , runStdoutLoggingT
                                                )
import           Control.Monad.Reader           ( ReaderT
                                                , runReaderT
                                                )
import           Database.Persist.Postgresql    ( ConnectionString
                                                , withPostgresqlConn
                                                )
import           Database.Persist.Sql           ( SqlBackend )

runActionT
  :: MonadUnliftIO m
  => ConnectionString
  -> ReaderT SqlBackend (LoggingT m) a
  -> m a
runActionT connectionString action =
  runStdoutLoggingT $ withPostgresqlConn connectionString $ \backend ->
    runReaderT action backend

runChunkedActionT
  :: MonadUnliftIO m
  => MomentumConfig
  -> ReaderT SqlBackend (LoggingT (ReaderT MomentumConfig m)) a
  -> m a
runChunkedActionT appConfig service =
  let connStr = connectionString appConfig
  in  runReaderT
        (runStdoutLoggingT $ withPostgresqlConn connStr $ runReaderT service)
        appConfig
