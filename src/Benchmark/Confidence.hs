{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}

-- |
module Benchmark.Confidence where

import           Benchmark.RegressionComparision
                                                ( multipleConfidenceWeights )
import           Control.Applicative            ( liftA2 )
import           Control.Monad.IO.Class         ( MonadIO )
import           Control.Monad.Reader           ( ReaderT )
import           Database.Persist.Sql           ( BackendCompatible
                                                , BaseBackend
                                                , PersistEntity
                                                , PersistEntityBackend
                                                , PersistQueryRead
                                                , SqlBackend
                                                )
import qualified Process.Confidence.LinearRegression
                                               as CLR
import           Retrive.Equity.Database        ( getListOfEquityFromEquityIdentifiers
                                                , getListOfHighVolumeActiveEquityIdentifiers
                                                )
import           Types.Confidence               ( Confidence
                                                , confidenceHistoricRor
                                                , confidenceMonthRor
                                                , confidenceYearRor
                                                )
import           Types.Equity                   ( Equity
                                                , equityClose
                                                )
import           Utility.Numeral                ( getRateOfReturn )

data Benchmark
  = CorrectWentUp
  | CorrectWentDown
  | IncorrectWentUp
  | IncorrectWentDown
  | Zero
  deriving (Show, Eq)

any2 :: (a -> Bool) -> [a] -> Bool
any2 fn list = length (filter fn list) >= 2

benchmarkPercentage :: [Benchmark] -> Double
benchmarkPercentage benchmarkList =
  (positiveWentDown + positiveWentUp) / total * 100
 where
  positiveWentDown =
    fromIntegral $ length $ filter (== CorrectWentDown) benchmarkList
  positiveWentUp =
    fromIntegral $ length $ filter (== CorrectWentUp) benchmarkList
  total = fromIntegral $ length $ filter (/= Zero) benchmarkList

benchmarkConfidence :: [Equity] -> ([Equity] -> Confidence) -> Benchmark
benchmarkConfidence equityList confidenceGenerator
  | goingUp == wentUp         = CorrectWentUp
  | goingUp && not wentUp     = IncorrectWentDown
  | not goingUp && wentUp     = IncorrectWentUp
  | not goingUp && not wentUp = CorrectWentDown
  | otherwise                 = Zero
 where
  pastEquityList       = drop 30 equityList
  pastConfidence       = confidenceGenerator pastEquityList
  predictedHistoricRoR = confidenceHistoricRor pastConfidence
  predictedYearRoR     = confidenceYearRor pastConfidence
  predictedMonthRoR    = confidenceMonthRor pastConfidence
  pastClose            = equityClose $ head pastEquityList
  actualClose          = equityClose $ head equityList
  actualRoR            = liftA2 getRateOfReturn pastClose actualClose
  goingUp =
    any2 (> pure 0) [predictedMonthRoR, predictedYearRoR, predictedHistoricRoR]
  wentUp = actualRoR > pure 0

getThresheldEquityList
  :: ( PersistQueryRead backend
     , MonadIO m
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     , BackendCompatible SqlBackend backend
     )
  => ReaderT backend m [[Equity]]
getThresheldEquityList = do
  highVolIdentifiers <- getListOfHighVolumeActiveEquityIdentifiers
  let top10 = take 10 highVolIdentifiers
  equityList <- getListOfEquityFromEquityIdentifiers top10
  return $ drop 100 <$> equityList

benchmarkLinearRegression
  :: ( PersistQueryRead backend
     , MonadIO m
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     , BackendCompatible SqlBackend backend
     )
  => ReaderT backend m Double
benchmarkLinearRegression = do
  thresheldEquityList <- getThresheldEquityList
  return
    $   benchmarkPercentage
    $   flip benchmarkConfidence CLR.generateConfidence
    <$> thresheldEquityList

benchmarkLinearRegressionWeightage
  :: ( PersistQueryRead backend
     , MonadIO m
     , PersistEntity Equity
     , PersistEntityBackend Equity ~ BaseBackend backend
     , BackendCompatible SqlBackend backend
     )
  => ReaderT backend m (Int, Int, Int, Int)
benchmarkLinearRegressionWeightage = do
  thresheldEquityList <- getThresheldEquityList
  return $ multipleConfidenceWeights thresheldEquityList
