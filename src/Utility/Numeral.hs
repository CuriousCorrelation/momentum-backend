-- |

module Utility.Numeral where

import           Data.Vector                    ( Vector )
import           Control.Applicative            ( liftA2 )
import           Statistics.LinearRegression    ( linearRegression )

-- | Sums up all list of values wrapped in 'Maybe' monad.
getTotal :: [Maybe Double] -> Maybe Double
getTotal = fmap sum . sequence

-- | Returns the length of a list of items as 'Double' wrapped in 'Maybe' mgetad.
getLength :: [a] -> Maybe Double
getLength = pure . fromIntegral . length

-- | Returns the average of a list of values wrapped in 'Maybe' monad.
getAverage :: [Maybe Double] -> Maybe Double
getAverage []     = Nothing
getAverage closes = liftA2 (/) (getTotal closes) (getLength closes)

-- | Intermidiate helper function for 'estimation'.
estimation
  :: (Double, Double) -- ^ Tuple of Alpha (Linear regression) and Beta (Linear regression).
  -> Double -- ^ Parameter (Value to predict for).
  -> Double  -- ^ Predicted dependent variable (Linear regression).
estimation (alpha, beta) ofValue = alpha + beta * ofValue

-- | Type constrained 'linearRegression'.
linearRegression'
  :: Vector Double -- ^ List of dependent variables (Linear regression).
  -> Vector Double  -- ^ List of independent variables (Linear regression).
  -> (Double, Double)  -- ^ Tuple of Alpha (Linear regression) and Beta (Linear regression).
linearRegression' = linearRegression

-- | Synonym of 'sequence' for tuple.
seqTuple :: Maybe (a1, a2) -> (Maybe a1, Maybe a2)
seqTuple Nothing       = (Nothing, Nothing)
seqTuple (Just (a, b)) = (Just a, Just b)

-- | Calculates the rate of return given an initial value and the current value.
getRateOfReturn
  :: Fractional a
  => a -- ^ Initial value.
  -> a -- ^ Current value.
  -> a -- ^ RoR
getRateOfReturn initial current = (current - initial) / initial * 100
