{-# LANGUAGE OverloadedStrings, TypeFamilies, GADTs, FlexibleContexts #-}
module PullAndCollect where

import Database.Persist
import Database.Persist.MySQL
import Data.Time
import Control.Monad.Trans.Resource (runResourceT)
import Control.Monad.Logger (runStderrLoggingT)
import Control.Monad.IO.Class (liftIO)

import LegacyTable

mysqlConn :: ConnectInfo
mysqlConn = defaultConnectInfo { 
    connectPassword = "arimasen", 
    connectDatabase = "pricer" }

day :: Day
day = fromGregorian 2013 06 25

prettyPrint :: [(Double, Double)] -> IO ()
prettyPrint = mapM_ (\(a,b) -> do
    putStr $ show a
    putStr ","
    print b
    )

mkTimeInt :: TimeOfDay -> Int
mkTimeInt = (*) 1000 . truncate . timeOfDayToTime

mkInt :: Tick -> Double
mkInt (Tick _ td ms _ _ _ _) = 
    fromIntegral $ (ms `div` 1000) + mkTimeInt td

durations :: Num a => [a] -> [a]
durations xs = map (\(a,b) -> b - a) tuples
    where
        tuples = zip xs (tail xs)

collectStats :: [Tick] -> [(Double, Double)]
collectStats ticks = zip durs (tail durs)
    where
        durs    = durations $ map mkInt ticks

main :: IO ()
main = withMySQLConn mysqlConn $ \conn ->
    runResourceT $ runStderrLoggingT $ flip runSqlConn conn $ do
        printMigration migrateAll
        runMigration migrateAll
        ticks <- selectList [TickDay ==. day] [Asc TickTime, Asc TickMksec]
        let stats = collectStats $ map entityVal ticks
        liftIO $ prettyPrint stats
        return ()